package com.DreamAp.external.api;

import com.DreamAp.external.api.ex.LoadingException;
import com.DreamAp.model.SwCharacter;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service("throwingDataLoader")
public class ThrowingDataLoaderTestImpl implements DataLoader {
	@Override public Set<SwCharacter> getSwCharacters() throws LoadingException {
		throw new LoadingException("Test exception");
	}
}
