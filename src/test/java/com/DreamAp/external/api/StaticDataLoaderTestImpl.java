package com.DreamAp.external.api;

import com.DreamAp.external.api.ex.LoadingException;
import com.DreamAp.model.Homeworld;
import com.DreamAp.model.Starship;
import com.DreamAp.model.SwCharacter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Set;

@Service("staticDataLoader")
public class StaticDataLoaderTestImpl implements DataLoader {
	private Homeworld homeworld1;
	private Homeworld homeworld2;

	private Starship starship1;
	private Starship starship2;

	private SwCharacter swCharacter1;
	private SwCharacter swCharacter2;
	private SwCharacter swCharacter3;
	private SwCharacter swCharacter4;

	private Set<SwCharacter> swCharacterSet;

	@PostConstruct
	private void setUpLocal() {
		createElements();
		swCharacterSet = Set.of(swCharacter1, swCharacter2, swCharacter3, swCharacter4);
	}

	private void createElements() {
		createHomeworlds();
		createStarships();
		createCharacters();
	}

	private void createCharacters() {
		swCharacter1 = new SwCharacter();
		swCharacter1.setName("swCharacter1");
		swCharacter1.setHomeworld(homeworld1);
		swCharacter1.setStarships(Set.of(starship1));

		swCharacter2 = new SwCharacter();
		swCharacter2.setName("swCharacter2");
		swCharacter2.setHomeworld(homeworld2);
		swCharacter2.setStarships(Set.of(starship1, starship2));

		swCharacter3 = new SwCharacter();
		swCharacter3.setName("swCharacter3");
		swCharacter3.setHomeworld(homeworld2);
		swCharacter3.setStarships(Set.of(starship2));

		swCharacter4 = new SwCharacter();
		swCharacter4.setName("SwCharacter4");
	}

	private void createStarships() {
		starship1 = new Starship();
		starship1.setName("starship1");

		starship2 = new Starship();
		starship2.setName("starship2");
	}

	private void createHomeworlds() {
		homeworld1 = new Homeworld();
		homeworld1.setName("homeworld1");

		homeworld2 = new Homeworld();
		homeworld2.setName("homeworld2");
	}

	@Override public Set<SwCharacter> getSwCharacters() throws LoadingException {
		return swCharacterSet;
	}
}
