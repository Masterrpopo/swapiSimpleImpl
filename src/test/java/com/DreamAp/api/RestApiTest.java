package com.DreamAp.api;

import com.DreamAp.Application;
import com.DreamAp.DbInitializer;
import com.DreamAp.api.dto.SwCharacterDto;
import com.DreamAp.api.dto.SwCharacterPageDto;
import com.DreamAp.external.api.StaticDataLoaderTestImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringJUnitConfig(classes = {Application.class, RestApi.class, StaticDataLoaderTestImpl.class, DbInitializer.class})
public class RestApiTest {

	@Autowired RestApi restApi;
	@Autowired Environment env;

	private final Logger logger = LogManager.getLogger();

	@Test
	public void correctPageTest() {
		ResponseEntity<SwCharacterPageDto> response = restApi.getPage(1);
		SwCharacterPageDto page = response.getBody();

		assertEquals(4, page.getCount().intValue());
	}

	@Test
	public void zeroIndexPageTest() {
		ResponseEntity<Status> response = restApi.getPage(0);
		assertEquals(env.getProperty("api.characters.page.invalidPageType"), response.getBody().getMessage());
	}

	@Test
	public void negativeIndexPageTest() {
		ResponseEntity<Status> response = restApi.getPage(-1);
		assertEquals(env.getProperty("api.characters.page.invalidPageType"), response.getBody().getMessage());
	}

	@Test
	public void exceedingIndexPageTest() {
		ResponseEntity<Status> response = restApi.getPage(5);
		assertEquals(String.format(env.getProperty("api.characters.page.notFound"), 5, 1), response.getBody().getMessage());
	}

	@Test
	public void correctCharacterTest() {
		ResponseEntity<SwCharacterDto> response = restApi.getCharacter(1);
		SwCharacterDto character = response.getBody();

		assertFalse(StringUtils.isEmpty(character.getName()));
	}

	@Test
	public void notExistingCharacterTest() {
		ResponseEntity<Status> response = restApi.getCharacter(-3);
		assertEquals(String.format(env.getProperty("api.characters.single.notFound"), -3), response.getBody().getMessage());
	}

	@Test
	public void minimalPerformanceTest() throws InterruptedException {
		Random random = new Random();
		ExecutorService executorService = Executors.newFixedThreadPool(20);

		Instant beginningTimestamp = Instant.now();

		executorService.invokeAll(Collections.nCopies(20, () -> {
			if (random.nextBoolean()) {
				return restApi.getPage(random.nextInt(4) - 1);
			} else {
				return restApi.getCharacter(random.nextInt(7) - 2);
			}
		}));

		executorService.shutdown();
		executorService.awaitTermination(5, TimeUnit.SECONDS);

		Instant endTime = Instant.now();

		long executionMilis = beginningTimestamp.until(endTime, ChronoUnit.MILLIS);
		logger.info("20 requests execution time in millis: {}", executionMilis);

		assertTrue(beginningTimestamp.plusSeconds(1).isAfter(endTime));
	}
}