package com.DreamAp;

import com.DreamAp.external.api.StaticDataLoaderTestImpl;
import com.DreamAp.external.api.ThrowingDataLoaderTestImpl;
import com.DreamAp.model.SwCharacter;
import com.DreamAp.repo.HomeworldsRepository;
import com.DreamAp.repo.StarshipsRepository;
import com.DreamAp.repo.SwCharactersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DbInitializerTest {

	@Autowired SwCharactersRepository swCharactersRepository;
	@Autowired HomeworldsRepository homeworldsRepository;
	@Autowired StarshipsRepository starshipsRepository;

	@Autowired AnnotationConfigApplicationContext context;

	@Test
	public void charactersPersistenceTest() {
		context.register(StaticDataLoaderTestImpl.class);
		context.register(DbInitializer.class);
		context.getBean(DbInitializer.class);

		SwCharacter probe = new SwCharacter();
		probe.setName("swCharacter2");

		Optional<SwCharacter> swCharacter2 = swCharactersRepository.findOne(Example.of(probe, ExampleMatcher.matchingAll().withIgnorePaths("id", "starships", "homeworld")));

		Assert.assertTrue(swCharacter2.isPresent());
	}

	@Test
	public void starshipsCascadePersistenceTest() {
		context.register(StaticDataLoaderTestImpl.class);
		context.register(DbInitializer.class);
		context.getBean(DbInitializer.class);

		Assert.assertEquals(2, starshipsRepository.count());
	}

	@Test
	public void homeworldsCascadePersistenceTest() {
		context.register(StaticDataLoaderTestImpl.class);
		context.register(DbInitializer.class);
		context.getBean(DbInitializer.class);

		Assert.assertEquals(2, homeworldsRepository.count());
	}

	@Test
	public void noExceptionOnFailedLoadingTest() {
		boolean exceptionThrown = false;
		try {
			context.register(ThrowingDataLoaderTestImpl.class);
			context.register(DbInitializer.class);
			context.getBean(DbInitializer.class);
		} catch (Exception e) {
			exceptionThrown = true;
		}

		Assert.assertFalse(exceptionThrown);
		Assert.assertEquals(0, swCharactersRepository.count());
	}
}