package com.DreamAp.repo;

import com.DreamAp.model.Homeworld;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeworldsRepository extends JpaRepository<Homeworld, Integer> {
}
