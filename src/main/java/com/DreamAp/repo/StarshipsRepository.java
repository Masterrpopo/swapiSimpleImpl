package com.DreamAp.repo;

import com.DreamAp.model.Starship;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StarshipsRepository extends JpaRepository<Starship, Integer> {
}
