package com.DreamAp.repo;

import com.DreamAp.model.SwCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SwCharactersRepository extends JpaRepository<SwCharacter, Integer> {
}
