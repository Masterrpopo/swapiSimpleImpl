package com.DreamAp.model;

import com.DreamAp.external.api.model.ExHomeworld;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "homeworld")
public class Homeworld {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "rotation_period")
	private String rotationPeriod;

	@Column(name = "orbital_period")
	private String orbitalPeriod;

	@Column(name = "diameter")
	private String diameter;

	@Column(name = "climate")
	private String climate;

	@Column(name = "gravity")
	private String gravity;

	@Column(name = "terrain")
	private String terrain;

	@Column(name = "surface_water")
	private String surfaceWater;

	@Column(name = "population")
	private String population;

	public static Homeworld fromExHomeworld(ExHomeworld exHomeworld) {
		Homeworld homeworld = new Homeworld();

		homeworld.setName(exHomeworld.getName());
		homeworld.setRotationPeriod(exHomeworld.getRotationPeriod());
		homeworld.setOrbitalPeriod(exHomeworld.getOrbitalPeriod());
		homeworld.setDiameter(exHomeworld.getDiameter());
		homeworld.setClimate(exHomeworld.getClimate());
		homeworld.setGravity(exHomeworld.getGravity());
		homeworld.setTerrain(exHomeworld.getTerrain());
		homeworld.setSurfaceWater(exHomeworld.getSurfaceWater());
		homeworld.setPopulation(exHomeworld.getPopulation());

		return homeworld;
	}
}
