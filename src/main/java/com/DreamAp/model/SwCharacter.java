package com.DreamAp.model;

import com.DreamAp.external.api.model.ExCharacter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "sw_character")
public class SwCharacter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "height")
	private String height;

	@Column(name = "mass")
	private String mass;

	@Column(name = "hair_color")
	private String hairColor;

	@Column(name = "skin_color")
	private String skinColor;

	@Column(name = "eye_color")
	private String eyeColor;

	@Column(name = "birth_year")
	private String birthYear;

	@Column(name = "gender")
	private String gender;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "homeworld_id")
	private Homeworld homeworld;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "character_x_starship",
			joinColumns = @JoinColumn(name = "character_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "starship_id", referencedColumnName = "id")
	)
	private Set<Starship> starships;

	public static SwCharacter fromExData(ExCharacter exCharacter, Homeworld homeworld, Set<Starship> starships) {
		SwCharacter swCharacter = new SwCharacter();

		swCharacter.setName(exCharacter.getName());
		swCharacter.setHeight(exCharacter.getHeight());
		swCharacter.setMass(exCharacter.getMass());
		swCharacter.setHairColor(exCharacter.getHairColor());
		swCharacter.setSkinColor(exCharacter.getSkinColor());
		swCharacter.setEyeColor(exCharacter.getEyeColor());
		swCharacter.setBirthYear(exCharacter.getBirthYear());
		swCharacter.setGender(exCharacter.getGender());
		swCharacter.setHomeworld(homeworld);
		swCharacter.setStarships(starships);

		return swCharacter;
	}
}
