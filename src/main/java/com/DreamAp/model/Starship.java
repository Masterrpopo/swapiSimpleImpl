package com.DreamAp.model;

import com.DreamAp.external.api.model.ExStarship;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "starship")
public class Starship {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "model")
	private String model;

	@Column(name = "manufacturer")
	private String manufacturer;

	@Column(name = "cost_in_credits")
	private String costInCredits;

	@Column(name = "length")
	private String length;

	@Column(name = "max_atmosphering_speed")
	private String maxAtmospheringSpeed;

	@Column(name = "crew")
	private String crew;

	@Column(name = "passengers")
	private String passengers;

	@Column(name = "cargo_capacity")
	private String cargoCapacity;

	@Column(name = "consumables")
	private String consumables;

	@Column(name = "hyperdrive_rating")
	private String hyperdriveRating;

	@Column(name = "mglt")
	private String mglt;

	@Column(name = "starship_class")
	private String starshipClass;

	public static Starship fromExStarship(ExStarship exStarship) {
		Starship starship = new Starship();

		starship.setName(exStarship.getName());
		starship.setModel(exStarship.getModel());
		starship.setManufacturer(exStarship.getManufacturer());
		starship.setCostInCredits(exStarship.getCostInCredits());
		starship.setLength(exStarship.getLength());
		starship.setMaxAtmospheringSpeed(exStarship.getMaxAtmospheringSpeed());
		starship.setCrew(exStarship.getCrew());
		starship.setPassengers(exStarship.getPassengers());
		starship.setCargoCapacity(exStarship.getCargoCapacity());
		starship.setConsumables(exStarship.getConsumables());
		starship.setHyperdriveRating(exStarship.getHyperdriveRating());
		starship.setMglt(exStarship.getMglt());
		starship.setStarshipClass(exStarship.getStarshipClass());

		return starship;
	}
}
