package com.DreamAp.external.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExCharacter {
	@JsonProperty("url")
	String url;

	@JsonProperty("name")
	String name;

	@JsonProperty("height")
	String height;

	@JsonProperty("mass")
	String mass;

	@JsonProperty("hair_color")
	String hairColor;

	@JsonProperty("skin_color")
	String skinColor;

	@JsonProperty("eye_color")
	String eyeColor;

	@JsonProperty("birth_year")
	String birthYear;

	@JsonProperty("gender")
	String gender;

	@JsonProperty("homeworld")
	String homeworldUrl;

	@JsonProperty("starships")
	String[] starshipsUrls;
}
