package com.DreamAp.external.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExHomeworld {
	@JsonProperty("url")
	String url;

	@JsonProperty("name")
	String name;

	@JsonProperty("rotation_period")
	String rotationPeriod;

	@JsonProperty("orbital_period")
	String orbitalPeriod;

	@JsonProperty("diameter")
	String diameter;

	@JsonProperty("climate")
	String climate;

	@JsonProperty("gravity")
	String gravity;

	@JsonProperty("terrain")
	String terrain;

	@JsonProperty("surface_water")
	String surfaceWater;

	@JsonProperty("population")
	String population;
}
