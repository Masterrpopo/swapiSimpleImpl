package com.DreamAp.external.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExStarship {
	@JsonProperty("url")
	private String url;

	@JsonProperty("name")
	private String name;

	@JsonProperty("model")
	private String model;

	@JsonProperty("manufacturer")
	private String manufacturer;

	@JsonProperty("cost_in_credits")
	private String costInCredits;

	@JsonProperty("length")
	private String length;

	@JsonProperty("max_atmosphering_speed")
	private String maxAtmospheringSpeed;

	@JsonProperty("crew")
	private String crew;

	@JsonProperty("passengers")
	private String passengers;

	@JsonProperty("cargo_capacity")
	private String cargoCapacity;

	@JsonProperty("consumables")
	private String consumables;

	@JsonProperty("hyperdrive_rating")
	private String hyperdriveRating;

	@JsonProperty("MGLT")
	private String mglt;

	@JsonProperty("starship_class")
	private String starshipClass;
}
