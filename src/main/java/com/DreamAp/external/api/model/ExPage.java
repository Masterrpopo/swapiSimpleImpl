package com.DreamAp.external.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExPage <T> {
	@JsonProperty("count")
	Integer count;

	@JsonProperty("next")
	String nextPageUrl;

	@JsonProperty("previous")
	String previousPageUrl;

	@JsonProperty("results")
	T[] results;
}
