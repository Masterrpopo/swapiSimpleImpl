package com.DreamAp.external.api;

import com.DreamAp.external.api.ex.LoadingException;
import com.DreamAp.external.api.model.*;
import com.DreamAp.model.Homeworld;
import com.DreamAp.model.Starship;
import com.DreamAp.model.SwCharacter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ExternalApiDataLoader implements DataLoader {
	private final Set<ExCharacter> exCharacters;
	private final Set<ExHomeworld> exHomeworlds;
	private final Set<ExStarship> exStarships;

	@Value("${swapi.host}")
	private String host;

	@Value("${swapi.resource.people}")
	private String peopleResource;

	@Value("${swapi.resource.homeworlds}")
	private String homeworldsResource;

	@Value("${swapi.resource.starships}")
	private String starshipsResource;

	private Logger logger = LogManager.getLogger();

	public ExternalApiDataLoader() {
		exCharacters = new HashSet<>();
		exHomeworlds = new HashSet<>();
		exStarships = new HashSet<>();
	}

	private void loadData() throws LoadingException {
		logger.info(">> Loading data from SWAPI");
		loadExCharacters();
		loadExHomeworlds();
		loadExStarships();
		logger.info("Data from SWAPI api loaded");
	}

	private void loadExCharacters() throws LoadingException {
		try {
			logger.debug(">> loadExCharacters()");
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(host + peopleResource);
			ExCharacterPage exCharacterPage = getExCharacterPage(httpClient, request);
			exCharacters.addAll(Arrays.asList(exCharacterPage.getResults()));

			while (exCharacterPage.getNextPageUrl() != null) {
				request = new HttpGet(exCharacterPage.getNextPageUrl());
				exCharacterPage = getExCharacterPage(httpClient, request);
				exCharacters.addAll(Arrays.asList(exCharacterPage.getResults()));
			}

			logger.debug("<< loadExCharacters()");
		} catch (IOException e) {
			throw new LoadingException(e);
		}
	}

	private void loadExHomeworlds() throws LoadingException {
		try {
			logger.debug(">> loadExHomeworlds()");

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(host + homeworldsResource);
			ExHomeworldPage exHomeworldPage = getExHomeworldPage(httpClient, request);
			exHomeworlds.addAll(Arrays.asList(exHomeworldPage.getResults()));

			while (exHomeworldPage.getNextPageUrl() != null) {
				request = new HttpGet(exHomeworldPage.getNextPageUrl());
				exHomeworldPage = getExHomeworldPage(httpClient, request);
				exHomeworlds.addAll(Arrays.asList(exHomeworldPage.getResults()));
			}

			logger.debug("<< loadExHomeworlds()");
		} catch (IOException e) {
			throw new LoadingException(e);
		}
	}

	private void loadExStarships() throws LoadingException {
		try {
			logger.debug(">> loadExStarships()");

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(host + starshipsResource);
			ExStarshipPage exStarshipPage = getExStarshipPage(httpClient, request);
			exStarships.addAll(Arrays.asList(exStarshipPage.getResults()));

			while (exStarshipPage.getNextPageUrl() != null) {
				request = new HttpGet(exStarshipPage.getNextPageUrl());
				exStarshipPage = getExStarshipPage(httpClient, request);
				exStarships.addAll(Arrays.asList(exStarshipPage.getResults()));
			}

			logger.debug("<< loadExStarships()");
		} catch (IOException e) {
			throw new LoadingException(e);
		}
	}

	private ExCharacterPage getExCharacterPage(HttpClient httpClient, HttpGet request) throws IOException {
		HttpResponse response = httpClient.execute(request);
		String entity = EntityUtils.toString(response.getEntity());
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(entity, ExCharacterPage.class);
	}

	private ExHomeworldPage getExHomeworldPage(HttpClient httpClient, HttpGet request) throws IOException {
		HttpResponse response = httpClient.execute(request);
		String entity = EntityUtils.toString(response.getEntity());
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(entity, ExHomeworldPage.class);
	}

	private ExStarshipPage getExStarshipPage(HttpClient httpClient, HttpGet request) throws IOException {
		HttpResponse response = httpClient.execute(request);
		String entity = EntityUtils.toString(response.getEntity());
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(entity, ExStarshipPage.class);
	}

	@Override public Set<SwCharacter> getSwCharacters() throws LoadingException {
		logger.info(">> getSwCharacters()");
		if (!isDataLoaded()) {
			loadData();
		}

		Set<SwCharacter> swCharacters = exCharacters.stream().map(exCharacter -> {
			ExHomeworld exHomeworld = exHomeworlds.stream().filter(homeworld -> homeworld.getUrl().equals(exCharacter.getHomeworldUrl())).findFirst().orElse(null);
			Homeworld homeworld = exHomeworld != null ? Homeworld.fromExHomeworld(exHomeworld) : null;

			Set<ExStarship> exStarships = getExStarships(exCharacter);
			Set<Starship> starships = exStarships.stream().map(Starship::fromExStarship).collect(Collectors.toSet());

			return SwCharacter.fromExData(exCharacter, homeworld, starships);
		}).collect(Collectors.toSet());

		logger.info("<< getSwCharacters : Found {} characters with {} different homeworlds and {} different starships", exCharacters.size(), exHomeworlds.size(), exStarships.size());
		return swCharacters;
	}

	private boolean isDataLoaded() {
		return exCharacters.size() != 0;
	}

	private Set<ExStarship> getExStarships(ExCharacter exCharacter) {
		Set<ExStarship> exCharacterStarships = new HashSet<>();
		for (String starshipUrl : exCharacter.getStarshipsUrls()) {
			ExStarship exStarship = exStarships.stream().filter(starship -> starship.getUrl().equals(starshipUrl)).findFirst().orElse(null);
			if (exStarship != null)
				exCharacterStarships.add(exStarship);
		}
		return exCharacterStarships;
	}
}
