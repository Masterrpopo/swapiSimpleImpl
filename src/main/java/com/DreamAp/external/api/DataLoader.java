package com.DreamAp.external.api;

import com.DreamAp.external.api.ex.LoadingException;
import com.DreamAp.model.SwCharacter;

import java.util.Set;

public interface DataLoader {
	Set<SwCharacter> getSwCharacters() throws LoadingException;
}
