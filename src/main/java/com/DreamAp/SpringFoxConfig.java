package com.DreamAp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.LinkedList;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.DreamAp"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo())
				.produces(Set.of("application/json"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
				"Swapi impl",
				"Api to get information about Star Wars characters, Homeworlds and Starships",
				"1.0",
				null,
				new Contact("Andrzej Pawłowski", "https://www.linkedin.com/in/andrzej-paw%C5%82owski-97840318b/", "andrzeju.pawlowski@gmail.com"),
				null,
				null,
				new LinkedList<>()
		);
	}
}
