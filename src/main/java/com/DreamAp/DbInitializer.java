package com.DreamAp;

import com.DreamAp.ex.DatabaseAlreadyInitializedException;
import com.DreamAp.external.api.DataLoader;
import com.DreamAp.external.api.ex.LoadingException;
import com.DreamAp.model.SwCharacter;
import com.DreamAp.repo.SwCharactersRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Set;

@Service
public class DbInitializer {

	@Autowired
	private SwCharactersRepository swCharactersRepository;

	@Autowired
	private DataLoader dataLoader;

	private final Logger logger = LogManager.getLogger();

	@PostConstruct
	private void initializeDb() {
		try {
			validateDatabaseIsNotInitialized();
			logger.info("Initializing database...");

			Set<SwCharacter> loadedCharacters = dataLoader.getSwCharacters();
			swCharactersRepository.saveAll(loadedCharacters);

			logger.info("Database initialized");
		} catch (LoadingException e) {
			logger.error("Couldn't initialize database", e);
		} catch (DatabaseAlreadyInitializedException e) {
			logger.info("Database is already initialized");
		}
	}

	private void validateDatabaseIsNotInitialized() throws DatabaseAlreadyInitializedException {
		if (swCharactersRepository.count() > 0) throw new DatabaseAlreadyInitializedException();
	}
}
