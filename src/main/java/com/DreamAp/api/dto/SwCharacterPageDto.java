package com.DreamAp.api.dto;

import com.DreamAp.model.SwCharacter;
import io.swagger.annotations.ApiModel;
import org.springframework.data.domain.Page;

@ApiModel
public class SwCharacterPageDto extends PageDto<SwCharacterDto> {
	public static SwCharacterPageDto fromPage(Page<SwCharacter> page) {
		SwCharacterPageDto pageDto = new SwCharacterPageDto();

		pageDto.setPages(page.getTotalPages());
		pageDto.setCount(Math.toIntExact(page.getTotalElements()));
		pageDto.setElements(page.getContent().stream().map(SwCharacterDto::fromSwCharacter).toArray(SwCharacterDto[]::new));

		return pageDto;
	}
}
