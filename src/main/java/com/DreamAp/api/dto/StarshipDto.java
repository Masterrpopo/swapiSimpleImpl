package com.DreamAp.api.dto;

import com.DreamAp.model.Starship;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class StarshipDto {
	@JsonProperty("name")
	private String name;

	@JsonProperty("model")
	private String model;

	@JsonProperty("manufacturer")
	private String manufacturer;

	@JsonProperty("costInCredits")
	private String costInCredits;

	@JsonProperty("length")
	private String length;

	@JsonProperty("maxAtmosphericSpeed")
	private String maxAtmosphericSpeed;

	@JsonProperty("crew")
	private String crew;

	@JsonProperty("passengers")
	private String passengers;

	@JsonProperty("cargoCapacity")
	private String cargoCapacity;

	@JsonProperty("consumables")
	private String consumables;

	@JsonProperty("hyperdriveRating")
	private String hyperdriveRating;

	@JsonProperty("mglt")
	private String mglt;

	@JsonProperty("starshipClass")
	private String starshipClass;

	public static StarshipDto fromStarship(Starship starship) {
		if (starship == null) {
			return null;
		} else {
			return new StarshipDtoBuilder()
					.name(starship.getName())
					.model(starship.getModel())
					.manufacturer(starship.getManufacturer())
					.costInCredits(starship.getCostInCredits())
					.length(starship.getLength())
					.maxAtmosphericSpeed(starship.getMaxAtmospheringSpeed())
					.crew(starship.getCrew())
					.passengers(starship.getPassengers())
					.cargoCapacity(starship.getCargoCapacity())
					.consumables(starship.getConsumables())
					.hyperdriveRating(starship.getHyperdriveRating())
					.mglt(starship.getMglt())
					.starshipClass(starship.getStarshipClass())
					.build();
		}
	}
}
