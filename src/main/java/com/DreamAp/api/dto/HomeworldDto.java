package com.DreamAp.api.dto;

import com.DreamAp.model.Homeworld;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class HomeworldDto {
	@JsonProperty("name")
	String name;

	@JsonProperty("rotationPeriod")
	String rotationPeriod;

	@JsonProperty("orbitalPeriod")
	String orbitalPeriod;

	@JsonProperty("diameter")
	String diameter;

	@JsonProperty("climate")
	String climate;

	@JsonProperty("gravity")
	String gravity;

	@JsonProperty("terrain")
	String terrain;

	@JsonProperty("surfaceWater")
	String surfaceWater;

	@JsonProperty("population")
	String population;

	public static HomeworldDto fromHomeworld(Homeworld homeworld) {
		if (homeworld == null) {
			return null;
		} else {
			return new HomeworldDtoBuilder()
					.name(homeworld.getName())
					.rotationPeriod(homeworld.getRotationPeriod())
					.orbitalPeriod(homeworld.getOrbitalPeriod())
					.diameter(homeworld.getDiameter())
					.climate(homeworld.getClimate())
					.gravity(homeworld.getGravity())
					.terrain(homeworld.getTerrain())
					.surfaceWater(homeworld.getSurfaceWater())
					.population(homeworld.getPopulation())
					.build();
		}
	}
}
