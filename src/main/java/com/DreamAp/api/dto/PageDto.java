package com.DreamAp.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class PageDto<T> {
	@JsonProperty("count")
	private Integer count;

	@JsonProperty("pages")
	private Integer pages;

	@JsonProperty("elements")
	private T[] elements;
}
