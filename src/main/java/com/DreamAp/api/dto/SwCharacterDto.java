package com.DreamAp.api.dto;

import com.DreamAp.model.SwCharacter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class SwCharacterDto {

	@JsonProperty("id")
	private Integer id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("height")
	private String height;

	@JsonProperty("mass")
	private String mass;

	@JsonProperty("hairColor")
	private String hairColor;

	@JsonProperty("skinColor")
	private String skinColor;

	@JsonProperty("eyeColor")
	private String eyeColor;

	@JsonProperty("birthYear")
	private String birthYear;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("homeworld")
	private HomeworldDto homeworldDto;

	@JsonProperty("starships")
	private StarshipDto[] starshipDtos;

	public static SwCharacterDto fromSwCharacter(SwCharacter swCharacter) {
		if (swCharacter == null) {
			return null;
		} else {
			return new SwCharacterDtoBuilder()
					.id(swCharacter.getId())
					.name(swCharacter.getName())
					.height(swCharacter.getHeight())
					.mass(swCharacter.getMass())
					.hairColor(swCharacter.getHairColor())
					.skinColor(swCharacter.getSkinColor())
					.eyeColor(swCharacter.getEyeColor())
					.birthYear(swCharacter.getBirthYear())
					.gender(swCharacter.getGender())
					.homeworldDto(HomeworldDto.fromHomeworld(swCharacter.getHomeworld()))
					.starshipDtos(swCharacter.getStarships().stream().map(StarshipDto::fromStarship).toArray(StarshipDto[]::new))
					.build();
		}
	}
}
