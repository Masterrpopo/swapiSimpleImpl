package com.DreamAp.api;

import com.DreamAp.api.dto.SwCharacterDto;
import com.DreamAp.api.dto.SwCharacterPageDto;
import com.DreamAp.api.ex.ArgumentOutOfRangeException;
import com.DreamAp.model.SwCharacter;
import com.DreamAp.repo.SwCharactersRepository;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
public class RestApi {

	public final int PAGE_SIZE;

	private Logger logger = LogManager.getLogger();

	@Autowired
	private SwCharactersRepository swCharactersRepository;

	@Autowired
	private Environment env;

	public RestApi(@Value("${api.page.size}") int pageSize) {
		PAGE_SIZE = pageSize;
	}

	@ApiResponses({
			@ApiResponse(code = 200, message = "Requested page of characters", response = SwCharacterPageDto.class),
			@ApiResponse(code = 416, message = "Page number value is either not positive or exceeds the number of available pages", response = Status.class)
	})
	@GetMapping(path = "/characters", produces = "application/json")
	public ResponseEntity getPage(
			@RequestParam(value = "page", defaultValue = "1")
			@ApiParam(allowableValues = "range[1, infinity]", value = "Characters page number") int pageNumber) {
		try {
			logger.info(">> getPage() : page={}", pageNumber);

			validatePageNumber(pageNumber);
			Page<SwCharacter> page = swCharactersRepository.findAll(PageRequest.of(pageNumber - 1, PAGE_SIZE, Sort.by("id")));
			verifyPageIsNotEmpty(page);
			ResponseEntity responseEntity = new ResponseEntity<>(SwCharacterPageDto.fromPage(page), HttpStatus.OK);

			logger.info("<< getPage()");
			return responseEntity;
		} catch (ArgumentOutOfRangeException e) {
			logger.warn("<< getPage() : {}", e.getMessage());
			return new ResponseEntity<>(new Status(e.getMessage()), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}

	private void validatePageNumber(int pageNumber) throws ArgumentOutOfRangeException {
		if (pageNumber < 1)
			throw new ArgumentOutOfRangeException(env.getProperty("api.characters.page.invalidPageType"));
	}

	private void verifyPageIsNotEmpty(Page<SwCharacter> page) throws ArgumentOutOfRangeException {
		if (page.getNumber() >= page.getTotalPages())
			throw new ArgumentOutOfRangeException(String.format(env.getProperty("api.characters.page.notFound"), page.getNumber() + 1, page.getTotalPages()));
	}

	@ApiResponses({
			@ApiResponse(code = 200, message = "Details of requested character", response = SwCharacterDto.class),
			@ApiResponse(code = 400, message = "Character with passed id does not exist", response = Status.class)
	})
	@GetMapping(path = "/characters/{id}", produces = "application/json")
	public ResponseEntity getCharacter(@PathVariable(name = "id") @ApiParam(value = "Requested character ID") int characterId) {
		logger.info(">> getCharacter() : id={}", characterId);

		Optional<SwCharacter> swCharacter = swCharactersRepository.findById(characterId);
		if (swCharacter.isPresent()) {
			logger.info("<< getCharacter()");
			return new ResponseEntity<>(SwCharacterDto.fromSwCharacter(swCharacter.get()), HttpStatus.OK);
		} else {
			logger.warn("<< getCharacter() : Character with id='{}' was not found", characterId);
			return new ResponseEntity<>(new Status(String.format(env.getProperty("api.characters.single.notFound"), characterId)), HttpStatus.BAD_REQUEST);
		}
	}
}
