package com.DreamAp.api.ex;

public class ArgumentOutOfRangeException extends Exception {
	public ArgumentOutOfRangeException() {
	}

	public ArgumentOutOfRangeException(String message) {
		super(message);
	}

	public ArgumentOutOfRangeException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArgumentOutOfRangeException(Throwable cause) {
		super(cause);
	}
}
