package com.DreamAp.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
class Status {
	@JsonProperty("message")
	private String message;

	public Status(String message) {
		this.message = message;
	}
}
