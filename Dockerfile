FROM maven:3.6.3-jdk-11
COPY . /usr/app

WORKDIR /usr/app

RUN apt-get install -y git
RUN git clone https://github.com/vishnubob/wait-for-it.git /script

ENTRYPOINT cd /usr/app \
&& mvn clean install -Pmysql-docker \
&& /script/wait-for-it.sh db:3306 \
&& cd target \
&& java -jar 'SwapiSimpleImpl-1.0.jar'
